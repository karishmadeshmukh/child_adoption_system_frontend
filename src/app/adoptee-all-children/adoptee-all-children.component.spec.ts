import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdopteeAllChildrenComponent } from './adoptee-all-children.component';

describe('AdopteeAllChildrenComponent', () => {
  let component: AdopteeAllChildrenComponent;
  let fixture: ComponentFixture<AdopteeAllChildrenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdopteeAllChildrenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdopteeAllChildrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
