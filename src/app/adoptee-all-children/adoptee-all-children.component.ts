import { ToastrService } from 'ngx-toastr';
import { AdopteeService } from './../adoptee.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Children } from './../children';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-adoptee-all-children',
  templateUrl: './adoptee-all-children.component.html',
  styleUrls: ['./adoptee-all-children.component.css']
})
export class AdopteeAllChildrenComponent implements OnInit {

  children: Observable<Children[]>;

  constructor(private route: ActivatedRoute, private router: Router,
    private adopteeService : AdopteeService, private toastr : ToastrService) { }

  ngOnInit(): void {
    console.log(`Inside adoptee-all-children.component.ts`);
    this.fetchRequestedChildrenList();
  }

  fetchRequestedChildrenList() {
    this.children = this.adopteeService.getChildrenList();
  }


  onSendReq(childId){
    console.log(`child id : ` + childId);
    this.adopteeService.sendRequest(childId,sessionStorage.getItem('adopteeid'))
      .subscribe(
        data => {
          console.log(data);
          this.toastr.success('Request sent successfully');
        },
        error => {
          console.log(error);
          this.toastr.error('Request failed')
      })
  }


  onLogout(){
    this.router.navigate(['/home']);
  }

}
