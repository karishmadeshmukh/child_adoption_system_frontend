import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    console.log("Inside home.component.ts");
  }

  goToAdmin(){
    this.router.navigate(['/adminviewchildren']);
    //this.router.navigate(['/employees']);
  }

  onLogout(){
    this.router.navigate(['/home']);
  }
}
