import { ToastrModule, ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { DonorService } from './../donor.service';
import { Children, ChildEntity } from './../children';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-child-registration',
  templateUrl: './child-registration.component.html',
  styleUrls: ['./child-registration.component.css']
})
export class ChildRegistrationComponent implements OnInit {

  child : ChildEntity = new ChildEntity();

  constructor(private donorService : DonorService,
    private router: Router, private toastr : ToastrService) { }

  ngOnInit(): void {
  }

  onRegister(){
    this.donorService.registerChild(this.child, sessionStorage.getItem('donorid'))
    .subscribe(data => {
      console.log(data)
      this.router.navigate(['/home']);
      this.toastr.success('Registered successfully')
    }, error => {
      console.log(error);
      this.toastr.error('Registration failed');
    });

  }

}
