import { TestBed } from '@angular/core/testing';

import { AdopteeService } from './adoptee.service';

describe('AdopteeService', () => {
  let service: AdopteeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdopteeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
