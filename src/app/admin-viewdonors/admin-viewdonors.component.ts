import { AdminService } from './../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Donor } from './../children';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-viewdonors',
  templateUrl: './admin-viewdonors.component.html',
  styleUrls: ['./admin-viewdonors.component.css']
})
export class AdminViewdonorsComponent implements OnInit {

  donors: Observable<Donor[]>;

  constructor(private route: ActivatedRoute, private router: Router,
    private adminService: AdminService) { }

  ngOnInit(): void {
    console.log(`Inside admin-viewdonors.component.ts`);
    this.fetchDonorsList();
  }

  fetchDonorsList() {
    this.donors = this.adminService.getDonorsList();
    console.log('Donors : ' + this.donors);
  }

  onEdit(){

  }

  onDelete(){
    
  }

  onLogout(){
    this.router.navigate(['/home']);
  }

}
