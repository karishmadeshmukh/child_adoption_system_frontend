import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewdonorsComponent } from './admin-viewdonors.component';

describe('AdminViewdonorsComponent', () => {
  let component: AdminViewdonorsComponent;
  let fixture: ComponentFixture<AdminViewdonorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminViewdonorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewdonorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
