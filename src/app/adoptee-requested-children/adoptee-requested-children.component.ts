import { ToastrService } from 'ngx-toastr';
import { AdopteeService } from './../adoptee.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Children } from './../children';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-adoptee-requested-children',
  templateUrl: './adoptee-requested-children.component.html',
  styleUrls: ['./adoptee-requested-children.component.css']
})
export class AdopteeRequestedChildrenComponent implements OnInit {

  children: Observable<Children[]>;

  constructor(private route: ActivatedRoute, private router: Router,
    private adopteeService : AdopteeService, private toastr : ToastrService) { }

  ngOnInit(): void {
    console.log(`Inside adoptee-requested-children.component.ts`);
    this.fetchRequestedChildrenList();
  }

  fetchRequestedChildrenList() {
    this.children = this.adopteeService.getRequestedChildrenList(sessionStorage.getItem('adopteeid'));
    //this.children = this.adopteeService.getChildrenList();
  }


  onConfirm(childId){
    console.log(`child id : ` + childId);
    this.adopteeService.confirmRequest(childId)
      .subscribe(
        data => {
          console.log(data);
          this.router.navigate(['/adoptedchildren']);
          this.toastr.success('Adoption successfull');
        },
        error => {
          console.log(error);
          this.toastr.error('Confirmation failed')
      })
  }

  onLogout(){
    this.router.navigate(['/home']);
  }


}
