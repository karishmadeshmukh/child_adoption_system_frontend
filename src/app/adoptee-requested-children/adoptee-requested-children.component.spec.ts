import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdopteeRequestedChildrenComponent } from './adoptee-requested-children.component';

describe('AdopteeRequestedChildrenComponent', () => {
  let component: AdopteeRequestedChildrenComponent;
  let fixture: ComponentFixture<AdopteeRequestedChildrenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdopteeRequestedChildrenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdopteeRequestedChildrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
