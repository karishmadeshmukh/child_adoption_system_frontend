import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './home/home.component';
import { AdminViewchildrenComponent } from './admin-viewchildren/admin-viewchildren.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { ToastrModule } from 'ngx-toastr';
import { AdminViewdonorsComponent } from './admin-viewdonors/admin-viewdonors.component';
import { AdminViewadopteesComponent } from './admin-viewadoptees/admin-viewadoptees.component';
import { AdminViewadoptedchildrenComponent } from './admin-viewadoptedchildren/admin-viewadoptedchildren.component';
import { DonorRegisterComponent } from './donor-register/donor-register.component';
import { DonorLoginComponent } from './donor-login/donor-login.component';
import { ChildRegistrationComponent } from './child-registration/child-registration.component';
import { AdopteeRegistrationComponent } from './adoptee-registration/adoptee-registration.component';
import { AdopteeLoginComponent } from './adoptee-login/adoptee-login.component';
import { AdopteeHomeComponent } from './adoptee-home/adoptee-home.component';
import { DonorHomeComponent } from './donor-home/donor-home.component';
import { AdopteeRequestedChildrenComponent } from './adoptee-requested-children/adoptee-requested-children.component';
import { AdopteeAdoptedChildrenComponent } from './adoptee-adopted-children/adoptee-adopted-children.component';
import { AdopteeAllChildrenComponent } from './adoptee-all-children/adoptee-all-children.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AdminViewchildrenComponent,
    AdminLoginComponent,
    AdminHomeComponent,
    AdminViewdonorsComponent,
    AdminViewadopteesComponent,
    AdminViewadoptedchildrenComponent,
    DonorRegisterComponent,
    DonorLoginComponent,
    ChildRegistrationComponent,
    AdopteeRegistrationComponent,
    AdopteeLoginComponent,
    AdopteeHomeComponent,
    DonorHomeComponent,
    AdopteeRequestedChildrenComponent,
    AdopteeAdoptedChildrenComponent,
    AdopteeAllChildrenComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
