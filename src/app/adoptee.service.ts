import { User, Children } from './children';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdopteeService {

  private baseUrl = 'http://localhost:7070';

  constructor(private http : HttpClient) { }

  registerAdoptee(adoptee: Object): Observable<Object> {
    return this.http.post(this.baseUrl+'/adoptee/register', adoptee);
  }

  signIn(user : User) : Observable<Object>{
    return this.http.post(this.baseUrl+'/adoptee/login', user);
  }

  getChildrenList(): Observable<any> {
    return this.http.get(this.baseUrl+'/adoptee/search');
  }

  sendRequest(childId : String, adopteeid : String) : Observable<Object> {
    console.log("Inside sendRequest() of AdopteeService");
    return this.http.get(this.baseUrl+'/adoptee/requestchild?adopteeid=' + adopteeid + '&childId=' + childId);
  }

  getRequestedChildrenList(adopteeid ) : Observable<any> {
    return this.http.get(this.baseUrl+'/adoptee/requestedchildren/' + adopteeid);
  }

  confirmRequest(childId) : Observable<Object>{
    return this.http.get(this.baseUrl+'/adoptee/confirmrequest/' + childId);
  } 

  getAdoptedChildrenList(adopteeid) : Observable<any>{
    return this.http.get(this.baseUrl+'/adoptee/adoptedchildrenbyadopteeid/' + adopteeid);
  }
}
