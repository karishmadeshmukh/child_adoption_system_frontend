import { Observable } from 'rxjs';
import { Children } from './../children';
import { AdminService } from './../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-viewchildren',
  templateUrl: './admin-viewchildren.component.html',
  styleUrls: ['./admin-viewchildren.component.css']
})
export class AdminViewchildrenComponent implements OnInit {

  children: Observable<Children[]>;

  constructor(private route: ActivatedRoute, private router: Router,
    private adminService: AdminService) { }

  ngOnInit(): void {
    console.log(`Inside admin-viewchildren.component.ts`);
    this.fetchChildrenList();

  }
  
  fetchChildrenList() {
    this.children = this.adminService.getChildrenList();
  }

  onLogout(){
    this.router.navigate(['/home']);
  }

  onEdit(){

  }

  onDelete(){
    
  }

}
