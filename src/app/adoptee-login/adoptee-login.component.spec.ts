import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdopteeLoginComponent } from './adoptee-login.component';

describe('AdopteeLoginComponent', () => {
  let component: AdopteeLoginComponent;
  let fixture: ComponentFixture<AdopteeLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdopteeLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdopteeLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
