import { ToastrService } from 'ngx-toastr';
import { AdopteeService } from './../adoptee.service';
import { Router } from '@angular/router';
import { Adoptee } from './../children';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-adoptee-login',
  templateUrl: './adoptee-login.component.html',
  styleUrls: ['./adoptee-login.component.css']
})
export class AdopteeLoginComponent implements OnInit {

  email = ""
  password = ""

  adoptee: Adoptee = new Adoptee();

  constructor(private router: Router,
              private adopteeService : AdopteeService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  onSignin(){
    this.adoptee.email = this.email;
    this.adoptee.password = this.password;
      this.adopteeService.signIn(this.adoptee)
        .subscribe(
          data => {
            console.log(data);
            sessionStorage['adopteeid'] = data['id'];
            //id =  sessionStorage.getItem('donorid');
            console.log(`id : ` + data['id']);
            this.router.navigate(['/adopteehome']);
            this.toastr.success('welcome', 'success');
            //this.toastr.success('welcome');
          },
          error => {
            console.log(error);
            this.router.navigate(['/adopteelogin']);
            this.toastr.error('Invalid credentials');
        })
  }

  onLogout(){
    sessionStorage.removeItem('adopteeid');
    this.router.navigate(['/home']);
  }

}
