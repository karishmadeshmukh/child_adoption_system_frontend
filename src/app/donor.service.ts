import { Observable } from 'rxjs';
import { Donor, User } from './children';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DonorService {

  private baseUrl = 'http://localhost:7070';

  constructor(private http : HttpClient) { }

  registerDonor(donor: Object): Observable<Object> {
    return this.http.post(this.baseUrl+'/donor/registerdonor', donor);
  }

  registerChild(child : Object, donorId : String): Observable<Object> {
    return this.http.post(this.baseUrl+'/donor/registerchildtry/' + donorId,child);
  }

  signIn(user : User) : Observable<Object>{
    return this.http.post(this.baseUrl+'/donor/login', user);
  }

  getDonatedChildrenList(donorId : String): Observable<any> {
    return this.http.get(this.baseUrl+'/donor/donatedchildren/' + donorId);
  }

  acceptRequest(childId : String) : Observable<Object> {
    return this.http.get(this.baseUrl+'/donor/acceptrequest/' + childId);
  }  

}
