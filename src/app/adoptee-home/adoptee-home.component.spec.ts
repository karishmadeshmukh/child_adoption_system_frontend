import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdopteeHomeComponent } from './adoptee-home.component';

describe('AdopteeHomeComponent', () => {
  let component: AdopteeHomeComponent;
  let fixture: ComponentFixture<AdopteeHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdopteeHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdopteeHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
