import { ToastrService } from 'ngx-toastr';
import { AdminService } from './../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AdopteeService } from './../adoptee.service';
import { Children } from './../children';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-adoptee-home',
  templateUrl: './adoptee-home.component.html',
  styleUrls: ['./adoptee-home.component.css']
})
export class AdopteeHomeComponent implements OnInit {

  
  children: Observable<Children[]>;

  constructor(private route: ActivatedRoute, private router: Router,
    private adopteeService : AdopteeService, private toastr : ToastrService) { }

  ngOnInit(): void {
    console.log(`Inside adoptee-home.component.ts`);
    this.fetchChildrenList();

  }
  
  fetchChildrenList() {
    this.children = this.adopteeService.getChildrenList();
  }

  onSendReq(childId){
    console.log(`Inside onSendReq() adoptee-home.component.ts`);
    console.log(`child id : ` + childId);
    console.log(`adopteeId : ` + sessionStorage.getItem('adopteeid'))
    console.log("Inside sendRequest() of adoptee-home-component : before sending req");
    this.adopteeService.sendRequest(childId, sessionStorage.getItem('adopteeid'))
        .subscribe(
          data => {
            console.log(data);
            this.toastr.success('Request sent successfully');
          },
          error => {
            console.log(error);
            this.toastr.error('Request failed')
        })
    console.log("Inside sendRequest() of adoptee-home-component : after sending req");
  }

  onLogout(){
    sessionStorage.removeItem('adopteeid');
    this.router.navigate(['/home']);
  }

}
