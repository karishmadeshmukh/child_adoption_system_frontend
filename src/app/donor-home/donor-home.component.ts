import { ToastrService } from 'ngx-toastr';
import { DonorService } from './../donor.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Children } from './../children';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/compiler/src/core';

@Component({
  selector: 'app-donor-home',
  templateUrl: './donor-home.component.html',
  styleUrls: ['./donor-home.component.css']
})
export class DonorHomeComponent implements OnInit {

  children: Observable<Children[]>;

  constructor(private route: ActivatedRoute, private router: Router,
    private donorService : DonorService, private toastr : ToastrService) { }

  ngOnInit(): void {
    //sessionStorage.getItem('donorid')
    console.log(`Inside admin-donorhome.component.ts`);
    this.fetchDonatedChildrenList();
  }

  fetchDonatedChildrenList() {
    this.children = this.donorService.getDonatedChildrenList(sessionStorage.getItem('donorid'));
    console.log(this.children)
    console.log(`Inside fetchDonatedChildrenList() of donor-home-component`);
  }

  onAcceptReq(childId){
    console.log(`child id : ` + childId);
    this.donorService.acceptRequest(childId)
        .subscribe(
          data => {
            console.log(data);
            this.router.navigate(['/donorhome']);
            this.toastr.success('Request accepted successfully');
          },
          error => {
            console.log(error);
            this.router.navigate(['/donorhome']);
            this.toastr.error('Accepting request failed')
        })
  }

  onLogout(){
    this.router.navigate(['/home']);
  }
}
