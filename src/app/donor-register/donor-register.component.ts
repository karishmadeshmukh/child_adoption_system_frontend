import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { DonorService } from './../donor.service';
import { Donor } from './../children';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-donor-register',
  templateUrl: './donor-register.component.html',
  styleUrls: ['./donor-register.component.css']
})
export class DonorRegisterComponent implements OnInit {

  donor : Donor = new Donor();

  constructor(private donorService : DonorService,
              private router: Router, private toastr : ToastrService) { }

  ngOnInit(): void {

  }

  onRegister(){
    this.donorService.registerDonor(this.donor)
    .subscribe(data => {
      console.log(data)
      this.router.navigate(['/donorlogin']);
      this.toastr.success('Registered successfully');
    }, error => {
      console.log(error);
      this.toastr.success('Registration Failed');
    });

  }

}
