import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdopteeAdoptedChildrenComponent } from './adoptee-adopted-children.component';

describe('AdopteeAdoptedChildrenComponent', () => {
  let component: AdopteeAdoptedChildrenComponent;
  let fixture: ComponentFixture<AdopteeAdoptedChildrenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdopteeAdoptedChildrenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdopteeAdoptedChildrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
