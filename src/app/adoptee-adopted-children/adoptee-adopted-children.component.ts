import { ToastrService } from 'ngx-toastr';
import { AdopteeService } from './../adoptee.service';
import { Children } from './../children';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-adoptee-adopted-children',
  templateUrl: './adoptee-adopted-children.component.html',
  styleUrls: ['./adoptee-adopted-children.component.css']
})
export class AdopteeAdoptedChildrenComponent implements OnInit {

  children : Observable<Children[]>;
  constructor(private route: ActivatedRoute, private router: Router,
    private adopteeService : AdopteeService, private toastr : ToastrService) { }

  ngOnInit(): void {
    console.log(`Inside adoptee-adopted-children.component.ts`);
    this.fetchAdoptedChildrenList();
  }

  fetchAdoptedChildrenList() {
    console.log(`Adoptee id : ` + sessionStorage.getItem('adopteeid'));
    this.children = this.adopteeService.getAdoptedChildrenList(sessionStorage.getItem('adopteeid'));
  }

  onLogout(){
    this.router.navigate(['/home']);
  }

}
