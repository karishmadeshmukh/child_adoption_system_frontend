import { AdminService } from './../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Adoptee } from './../children';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-viewadoptees',
  templateUrl: './admin-viewadoptees.component.html',
  styleUrls: ['./admin-viewadoptees.component.css']
})
export class AdminViewadopteesComponent implements OnInit {

  adoptees: Observable<Adoptee[]>;

  constructor(private route: ActivatedRoute, private router: Router,
    private adminService: AdminService) { }

  ngOnInit(): void {
    console.log(`Inside admin-viewdonors.component.ts`);
    this.fetchAdopteesList();
  }

  fetchAdopteesList() {
    this.adoptees = this.adminService.getAdopteesList();
  }

  onEdit(){

  }

  onDelete(){
    
  }

  onLogout(){
    this.router.navigate(['/home']);
  }

}
