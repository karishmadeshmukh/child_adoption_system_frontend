import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewadopteesComponent } from './admin-viewadoptees.component';

describe('AdminViewadopteesComponent', () => {
  let component: AdminViewadopteesComponent;
  let fixture: ComponentFixture<AdminViewadopteesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminViewadopteesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewadopteesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
