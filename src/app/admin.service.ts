import { User } from './children';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private baseUrl = 'http://localhost:7070';

  constructor(private http: HttpClient) { }

  getChildrenList(): Observable<any> {
    return this.http.get(this.baseUrl+'/admin/viewchildren');
  }

  getDonorsList(): Observable<any> {
    return this.http.get(this.baseUrl+'/admin/viewdonors');
  }

  getAdopteesList(): Observable<any> {
    return this.http.get(this.baseUrl+'/admin/viewadoptees');
  }

  getAdoptedChildrenList(): Observable<any> {
    return this.http.get(this.baseUrl+'/admin/viewadoptedchildren');
  }

  signIn(user : User) : Observable<Object>{
    return this.http.post(this.baseUrl+'/admin/login', user);

  }

}
