import { User } from './../children';
import { AdminService } from './../admin.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  email = ""
  password = ""

  user: User = new User();

  constructor(private router: Router,
              private adminService : AdminService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    console.log( `inside ngOnInit of AdminLoginComponent`)
  }

  onSignin(){
    this.user.email = this.email;
    this.user.password = this.password;
      this.adminService.signIn(this.user)
        .subscribe(
          data => {
            console.log(data);
            this.router.navigate(['/adminhome']);
            this.toastr.success('welcome', 'success');
            //this.toastr.success('welcome');
          },
          error => {
            console.log(error);
            this.router.navigate(['/adminlogin']);
            this.toastr.error('Invalid credentials');
        })
  }

  onLogout(){
    this.router.navigate(['/home']);
  }
}
