import { AdopteeAdoptedChildrenComponent } from './adoptee-adopted-children/adoptee-adopted-children.component';
import { AdopteeRequestedChildrenComponent } from './adoptee-requested-children/adoptee-requested-children.component';
import { DonorHomeComponent } from './donor-home/donor-home.component';
import { AdopteeHomeComponent } from './adoptee-home/adoptee-home.component';
import { AdopteeLoginComponent } from './adoptee-login/adoptee-login.component';
import { AdopteeRegistrationComponent } from './adoptee-registration/adoptee-registration.component';
import { ChildRegistrationComponent } from './child-registration/child-registration.component';
import { DonorLoginComponent } from './donor-login/donor-login.component';
import { AdminViewadoptedchildrenComponent } from './admin-viewadoptedchildren/admin-viewadoptedchildren.component';
import { AdminViewadopteesComponent } from './admin-viewadoptees/admin-viewadoptees.component';
import { AdminViewdonorsComponent } from './admin-viewdonors/admin-viewdonors.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminViewchildrenComponent } from './admin-viewchildren/admin-viewchildren.component';
import { HomeComponent } from './home/home.component';
import { DonorRegisterComponent } from './donor-register/donor-register.component';
import { AdopteeAllChildrenComponent } from './adoptee-all-children/adoptee-all-children.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'adminviewchildren', component: AdminViewchildrenComponent },
  { path: 'adminlogin', component: AdminLoginComponent},
  { path: 'adminhome', component : AdminHomeComponent},
  { path: 'adminviewdonors', component : AdminViewdonorsComponent},
  { path: 'adminviewadoptees', component : AdminViewadopteesComponent},
  { path: 'adminviewadoptedchildren', component : AdminViewadoptedchildrenComponent},
  { path: 'donorregister', component : DonorRegisterComponent},
  { path: 'donorlogin', component : DonorLoginComponent},
  { path: 'childregister', component : ChildRegistrationComponent},
  { path: 'adopteeregister', component : AdopteeRegistrationComponent},
  { path: 'adopteelogin', component : AdopteeLoginComponent},
  { path: 'adopteehome', component : AdopteeHomeComponent},
  { path: 'donorhome', component : DonorHomeComponent},
  { path: 'requestedchildren', component : AdopteeRequestedChildrenComponent},
  { path: 'adoptedchildren', component : AdopteeAdoptedChildrenComponent},
  { path: 'allchildren', component : AdopteeAllChildrenComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
