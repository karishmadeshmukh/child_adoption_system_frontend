import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewadoptedchildrenComponent } from './admin-viewadoptedchildren.component';

describe('AdminViewadoptedchildrenComponent', () => {
  let component: AdminViewadoptedchildrenComponent;
  let fixture: ComponentFixture<AdminViewadoptedchildrenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminViewadoptedchildrenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewadoptedchildrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
