import { AdminService } from './../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Children } from './../children';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-viewadoptedchildren',
  templateUrl: './admin-viewadoptedchildren.component.html',
  styleUrls: ['./admin-viewadoptedchildren.component.css']
})
export class AdminViewadoptedchildrenComponent implements OnInit {

  adoptedChildren: Observable<Children[]>;

  constructor(private route: ActivatedRoute, private router: Router,
    private adminService: AdminService) { }

  ngOnInit(): void {
    console.log(`Inside admin-viewadoptedchildren.component.ts`);
    this.fetchAdoptedChildrenList();
  }

  fetchAdoptedChildrenList() {
    this.adoptedChildren = this.adminService.getAdoptedChildrenList();
  }

  onEdit(){

  }

  onDelete(){
    
  }

  onLogout(){
    this.router.navigate(['/home']);
  }

}
