export class Children {
    childId : Number;
	name : String;
	birthDate : Date;
	age : Number;
	gender : String;
	image : String;
	donor : Donor;
	adoptee : Adoptee;
    isAdopted : Boolean;
	report : Boolean;
	requested : Number;
	reqaccepted : Number;
	confirmed : Number;
}

export class Donor{
    id : Number;
    password : String;
    saltPassword : String;
    name : String;
    email : String;
    mobile : String;
    gender : String;
    city : String;
    state : String;
    pincode : String;
    relation : String;
    reason : String;
}

export class Adoptee{
    id : Number;
	password : String;
	saltPassword : String;
	name : String;
	email : String;
	mobile : String;
	gender : String;
	maritalStatus : String;
	noOfBiologicalChildren : Number;
	noOfAdoptedChildren : Number;
	reason : String;
	city : String;
	state : String;
	pincode : String;
	annualIncome : Number;
}

export class User{
	email : String;
  	password : String;
}

export class ChildEntity{
	name : String;
	birthDate : Date;
	age : Number;
	gender : String;
	image : String;
	isAdopted : Boolean;
	report : Boolean;
	weight : Number;
	height : Number;
	bloodGroup : String;
	hb : Number;
	mentalHealthStatus : String;
	physicalHealthStatus : String;
	degree : String;
	institute : String;
}
