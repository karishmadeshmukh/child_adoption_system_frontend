import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AdopteeService } from './../adoptee.service';
import { Adoptee } from './../children';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-adoptee-registration',
  templateUrl: './adoptee-registration.component.html',
  styleUrls: ['./adoptee-registration.component.css']
})
export class AdopteeRegistrationComponent implements OnInit {

  adoptee : Adoptee = new Adoptee();

  constructor(private adopteeService : AdopteeService,
              private router: Router, private toastr : ToastrService) { }

  ngOnInit(): void {
  }

  onRegister(){
    this.adopteeService.registerAdoptee(this.adoptee)
    .subscribe(data => {
      console.log(data)
      this.router.navigate(['/adopteelogin']);
      this.toastr.success('Registered successfully');
    }, error => {
      console.log(error);
      this.toastr.success('Registration Failed');
    });

  }

}
