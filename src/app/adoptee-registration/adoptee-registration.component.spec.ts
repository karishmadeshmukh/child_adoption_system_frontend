import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdopteeRegistrationComponent } from './adoptee-registration.component';

describe('AdopteeRegistrationComponent', () => {
  let component: AdopteeRegistrationComponent;
  let fixture: ComponentFixture<AdopteeRegistrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdopteeRegistrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdopteeRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
