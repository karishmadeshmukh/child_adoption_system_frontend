import { DonorService } from './../donor.service';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from './../admin.service';
import { Router } from '@angular/router';
import { User, Donor } from './../children';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-donor-login',
  templateUrl: './donor-login.component.html',
  styleUrls: ['./donor-login.component.css']
})
export class DonorLoginComponent implements OnInit {

  email = ""
  password = ""

  donor: Donor = new Donor();

  constructor(private router: Router,
              private donorService : DonorService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  onSignin(){
    this.donor.email = this.email;
    this.donor.password = this.password;
      this.donorService.signIn(this.donor)
        .subscribe(
          data => {
            console.log(data);
            sessionStorage['donorid'] = data['id'];
            //id =  sessionStorage.getItem('donorid');
            console.log(`id : ` + data['id']);
            this.router.navigate(['/donorhome']);
            this.toastr.success('welcome', 'success');
            //this.toastr.success('welcome');
          },
          error => {
            console.log(error);
            this.router.navigate(['/donorlogin']);
            this.toastr.error('Invalid credentials');
        })
  }

  onLogout(){
    sessionStorage.removeItem('donorid');
    this.router.navigate(['/home']);
  }
}
